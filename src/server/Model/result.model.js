const { result } = require('lodash');
const mongoose = require('mongoose');
const shortid = require('shortid');
const Schema = mongoose.Schema;

const QuestionSchema = new Schema({
    id: String,
    answer: Number
});

const ResultSchema = new Schema({
    _id: {
        type: String,
        default: shortid.generate
    },
    email: {
        type: String,
        trim: true
    },
    questions: {
        type: QuestionSchema
    },

    mbit: { type: String }
});

module.exports = mongoose.model('Result', ResultSchema);
