import axios from 'axios';
import config from "./config/index";

export const sendQuizResponse = async (answers, email) => {
    const response = await axios.post(config.apiUrl + "/api/submitResponse", {
        answers,
        email
    });
    if (response.status === 200)
        window.location.href = `/Result/${response.data._id}`;
}

export const getResults = async (id) => {
    return axios.post(config.apiUrl + "/api/getResults", { id });
}