import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { Button, Container, Grid, GridColumn, Header } from 'semantic-ui-react';
import { getResults } from '../apiCalls';

class Result extends Component {
  state = { data: {}, redirectToQuiz : false }
  componentDidMount() {
    let { id } = this.props.match.params;
    getResults(id).then(({ data }) => {
      this.setState({ data });
    });
  }
  renderProspectiveView = (isLeft = false) => {
    const colorLeft = isLeft ? "#A920CB" : "#E9ECEF";
    const colorBox = isLeft ? "#E9ECEF" : "#A920CB";
    return <div className="rectangle" style={{ backgroundColor: colorBox }}>
      <div className="rect1" style={{ backgroundColor: colorLeft }} />
      <div className="rect2" />
    </div>
  }
  render() {
    let { id } = this.props.match.params;
    const {
      email = "",
      mbit = ""
    } = this.state.data;
    if (!id || this.state.redirectToQuiz) return <Redirect to="/" />

    return (
      <Container>
        <Header color="green" as="h2">Welcome {email}</Header>
        

        
        <Grid textAlign="center">
          <Grid.Row className="result">
            <Grid.Column width={4} className="header">
              <Header color="blue" as="h2">Your Prospective</Header>
              <span>Your Prospective type is {mbit}</span>
            </Grid.Column>
            <Grid.Column width={10}>
            <Grid textAlign="center" className ="bar">
              <GridColumn width={4}>Extraversion(E)</GridColumn>
              <GridColumn  width={5}>{this.renderProspectiveView(mbit.includes("E"))} </GridColumn>
              <GridColumn  width={4}>Introversion(I)</GridColumn>
              </Grid>  
              <Grid textAlign="center" className ="bar">
              <GridColumn width={4}>Sensing (S) </GridColumn>
              <GridColumn  width={5}>{this.renderProspectiveView(mbit.includes("S"))} </GridColumn>
              <GridColumn  width={4}> Intuition (N)</GridColumn>
              </Grid>  
              <Grid textAlign="center" className ="bar">
              <GridColumn width={4}>Thinking (T)</GridColumn>
              <GridColumn  width={5}>{this.renderProspectiveView(mbit.includes("T"))} </GridColumn>
              <GridColumn  width={4}>Feeling (F)</GridColumn>
              </Grid>  
              <Grid textAlign="center" className ="bar">
              <GridColumn width={4}>Judging (J) </GridColumn>
              <GridColumn  width={5}>{this.renderProspectiveView(mbit.includes("J"))} </GridColumn>
              <GridColumn  width={4}>Perceiving (P)</GridColumn>
              </Grid>  

            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Button primary onClick={() => this.setState({redirectToQuiz : true})}>New Quiz</Button>
          </Grid.Row>
        </Grid>

      </Container>
    );
  }
}

export default withRouter(Result);

