import { sum } from 'lodash';
import React, { Component } from 'react';
import { Button, Container, Grid, Header, Input, Radio } from "semantic-ui-react";
import questions from "../../../Data/questions.json";
import { SemanticToastContainer, toast } from 'react-semantic-toasts';
import { sendQuizResponse } from "../apiCalls";
export default class Home extends Component {
    state = {
        answers: {
            
            1: 3,
            2: 2,
            3: 6,
            4: 1,
            5: 7,
            6: 3,
            7: 2,
            8: 5,
            9: 2,
            10:7,
        },
        email: "chauhangaurav101@gmail.com"
    };
    handleChange = id => (value) => {
        const answers = { ...this.state.answers };
        answers[id] = value;
        this.setState({ answers });
    }
    onSubmit = () => {
        let hasError = null;
        const { answers } = this.state;
        questions.map(({ id }) => {
            if (answers[id] === undefined) {
                hasError = "Please complete all the questions.";
            }
        });
        if (!hasError) {
            if (_.isEmpty(this.state.email)) {
                hasError = "Please Enter a valid email.";
            }
        }
        if (hasError) {
            toast({
                type: 'error',
                icon: 'envelope',
                title: hasError,
                animation: 'bounce',
                time: 1000
            });
            return;
        }
        sendQuizResponse(answers, this.state.email);
    }
    renderQuestion = ({ question, id }) => {
        const options = [1, 2, 3, 4, 5, 6, 7];
        const answers = { ...this.state.answers };
        return (<Grid key={id} textAlign='center' className="quiz">
            <Grid.Row>
                <Grid.Column><Header as='h3'>{question}</Header></Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column>
                    <Header color='red' width={2} as="h4">Disagree</Header>
                </Grid.Column>
                {options.map(opt =>
                    <Grid.Column width={1} key={`Q${id}_O${opt}`}>
                        <Radio
                            value={answers[id] === opt}
                            checked={answers[id] === opt}
                            onChange={() => this.handleChange(id)(opt)}
                        />
                    </Grid.Column>
                )}
                <Grid.Column>
                    <Header color='green' width={2} as="h4">Agree</Header>
                </Grid.Column>
            </Grid.Row>

        </Grid>);
    }
    render() {
        return (<div>
            <Container>
                <Grid textAlign="left">
                    <Grid.Row>
                        <Grid.Column><Header as='h2'>Discover Your Prospective</Header>
                            <span>Complete the 7min test and get a detail report of your lonses on the world</span>

                        </Grid.Column>

                    </Grid.Row>

                </Grid>
                {questions.map(question => this.renderQuestion(question))}
                <SemanticToastContainer />

                <Grid textAlign="center" className="quiz">
                    <Grid.Row>
                        <Header as='h3'>Your Email</Header>
                    </Grid.Row>
                    <Grid.Row >
                        <Input
                            size="massive"
                            placeholder="you@example.com"
                            value={this.state.email}
                            onChange={(e, { value }) => this.setState({ email: value })} /></Grid.Row>
                </Grid>

                <Grid textAlign="center">
                    <Grid.Row>
                        <Button
                            primary size="huge"
                            onClick={() => this.onSubmit()}

                        > Save and Continue</Button>
                    </Grid.Row>
                </Grid>

            </Container>
        </div>);
    }
}

