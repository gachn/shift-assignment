const express = require('express');
const cors = require('cors');
const routes = require("./routes");
const mongoose = require('mongoose');
const config = require('./config/index');

const app = express();

app.use(cors())
app.use(express.json());

app.use(express.static('dist'));
app.use('/api/', routes);
connect();

function listen() {
    app.listen(
        process.env.PORT || 8080,
        () => console.log(`Listening on port ${process.env.PORT || 8080}!`)
    );
}

function connect() {
    mongoose.connection
        .on('error', console.log)
        .on('disconnected', connect)
        .once('open', listen);
    return mongoose.connect(config.db, {
        keepAlive: 1,
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}
