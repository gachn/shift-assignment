const express = require('express');
const resultController = require('./Controller/result.controller');
const app = express();

app.post('/healthy', (req, res) => res.send("Healthy"));

app.post('/submitResponse', resultController.saveResponse);
app.post('/getResults', resultController.getResults);

module.exports = app;