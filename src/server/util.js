function reverseString(str) {
    return str.split("").reverse().join("");
}
module.exports.calculateMBTI = (questions, answers) => {
    const mbit = { E: 0, I: 0, S: 0, N: 0, T: 0, F: 0, J: 0, P: 0 };
    questions.map(ques => {
        let {
            id,
            direction,
            dimension
        } = { ...ques };
        // reversing direction if dimention is reverse
        if (direction === -1) dimension = reverseString(dimension)
        let leadTowards = 0;
        if (answers[id] > 4) leadTowards = 1;
        mbit[dimension[leadTowards]]++;
    });
    
    let finalMbit = "";
    finalMbit += mbit["E"] >= mbit["I"] ? "E" : "I";
    finalMbit += mbit["S"] >= mbit["N"] ? "S" : "N";
    finalMbit += mbit["T"] >= mbit["F"] ? "T" : "F";
    finalMbit += mbit["J"] >= mbit["P"] ? "J" : "P";
    return finalMbit;
}