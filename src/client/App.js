import React, { Component } from 'react';
import './app.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Quiz from './Modules/Quiz';
import Result from './Modules/Result';
import _ from "lodash";


import 'semantic-ui-css/semantic.min.css'

const routes = [
  {
    path: '/',
    module: <Quiz />,
    exact: true
  },
  {
    path: '/quiz',
    module: <Quiz />
  },
  {
    path: '/result/:id',
    module: <Result />
  }
]
export default class App extends Component {
  render() {
    return (
      <div className="center">
        <Router>

          <Switch>
            {routes.map((route, index) =>
              <Route key={index}
                exact={_.get(route, 'exact', false)}
                path={route.path}
              >{route.module}</Route>)}
          </Switch>
        </Router>
      </div>
    );
  }
}
