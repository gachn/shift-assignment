const { result } = require("lodash");
const questions = require("../../../Data/questions.json");
const ResultModel = require("../Model/result.model");
const _ = require("lodash")
const { calculateMBTI } = require("../util");

module.exports.saveResponse = (req, res) => {
    const { answers, email } = req.body;
    // validation
    let hasError = null;
    const quesFormat = [];

    if (_.isNil(email)) hasError = "Invalid Email";
    questions.map(({ id }) => {
        if (_.isNil(answers[id]) || (answers[id] < 1 && answers[id] > 7)) {
            hasError = "Please check the answers";
        }
        quesFormat.push({
            id,
            answers: answers[id]
        });
    });
    if (hasError) return res.status(400).send(hasError);
    const mbit = calculateMBTI(questions, answers);
    const result = new ResultModel({ answers, email, mbit })
    result.save(err => {
        if (err) return res.status(500).send({ errMessage: err });
        return res.send(result);
    });
}

module.exports.getResults = (req, res) => {
    const { id } = req.body;
    ResultModel.findById(id).then(data => res.send(data)).catch(err => res.status(500).send(err));
}